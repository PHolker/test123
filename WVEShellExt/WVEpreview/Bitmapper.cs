﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;


namespace WVEThumbnail
{
    public class Bitmapper
    {
    //    public static Bitmap resizeImage(Image imgToResize, Size size)
    //{
    //   return (Image)(new Bitmap(imgToResize, size));
    //}

        //private void CallTheMethod(MemoryStream memStream)
        //{
        //    byte[] rawData = new byte[memStream.Length];
        //    memStream.Read(rawData, 0, memStream.Length);

        //    GCHandle rawDataHandle = GCHandle.Alloc(rawData, GCHandleType.Pinned);
        //    IntPtr address = handle.AddrOfPinnedObject();

        //    doSomething(address, rawData.Length);
        //    rawDataHandle.Free();
        //}

        public static Bitmap ResizeBitmap(Bitmap img, int maxWidth, int maxHeight)
        {
            //compute correct new size
            int ResizeWidth = img.Width;
            int ResizeHeight = img.Height;

            double aspect = ResizeWidth / ResizeHeight;

            if (ResizeWidth > maxWidth)
            {
                ResizeWidth = maxWidth;
                ResizeHeight = (int)(ResizeWidth / aspect);
            }
            if (ResizeHeight > maxHeight)
            {
                aspect = ResizeWidth / ResizeHeight;
                ResizeHeight = maxHeight;
                ResizeWidth = (int)(ResizeHeight * aspect);
            }
            return new Bitmap(img, ResizeWidth, ResizeHeight);
        }

        /// <summary>
        /// Methode erzeugt Bitmap
        /// </summary>
        public static Bitmap CreateBMPSource(double[,] Feld, double min, double max)
        {
            FalseColors m_falseColor = new FalseColors();

            m_falseColor.P_SetMaxZ = max;
            m_falseColor.P_SetMinZ = min;


            int resetsize = 32;
            if (Feld == null)
                //erzeuge leeres Bitmap der Größe 32x32, falls kein Feld vorhanden (reset)
                return new Bitmap(resetsize, resetsize);
            //else
            int height = Feld.GetLength(0); // flipped, since rectangular wve appear scrambled
            int width = Feld.GetLength(1);
            Bitmap bmp_source = new Bitmap(width, height, PixelFormat.Format32bppArgb); // Original Größe vom wve
            Color C_Ret;

            //SetPixel is deadslow! - LockBits technique is used instead
            int pixelsize = 4;
            //if (bmp_source.PixelFormat == PixelFormat.Format24bppRgb)
            //    pixelsize = 3;
            if (bmp_source.PixelFormat == PixelFormat.Format32bppArgb || bmp_source.PixelFormat == PixelFormat.Format32bppRgb)
                pixelsize = 4;
            else
                return bmp_source;


            BitmapData bmd = bmp_source.LockBits(new Rectangle(0, 0, bmp_source.Width, bmp_source.Height), ImageLockMode.ReadWrite, bmp_source.PixelFormat);
            int stride = bmd.Stride;
            System.IntPtr Scan0 = bmd.Scan0;
            try
            {
                unsafe
                {
                    byte* p = (byte*)(void*)Scan0;
                    int nOffset = stride - bmp_source.Width * pixelsize;
                    int nWidth = bmp_source.Width * pixelsize;

                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            //bisher via SetPixel ... sehr sehr langsam!
                            //Z_Wert = this.m_Feld._zWert[y, x];
                            //bmp_source.SetPixel(x, this.iHeight - 1 - y, C_Ret); // Color.White);//

                            // Ermittle Falschfarben

                            C_Ret = m_falseColor.GetColor(Feld[height - 1 - y, x]);
                            //C_Ret = m_falseColor.GetColor(this.m_Feld._zWert[this.iHeight - 1 - y, x]);
                            //setze BlueGreenRed(Alpha) bits des in (A)RGB codierten Pixels
                            p[0] = (byte)C_Ret.B;
                            p[1] = (byte)C_Ret.G;
                            p[2] = (byte)C_Ret.R;
                            if (pixelsize == 4) p[3] = (byte)255; //[0,255]->[transparent,opaque]
                            //move to next pixel in same row
                            p += pixelsize;
                        }
                        p += nOffset; //move to next row
                    }

                    //}
                    //else
                    //{   // keine Daten vorhanden
                    //    for (int y = 0; y < resetsize; y++)
                    //    {
                    //        for (int x = 0; x < resetsize; x++)
                    //        {
                    //            p[0] = (byte)255;
                    //            p[1] = (byte)255;
                    //            p[2] = (byte)255;
                    //            if (pixelsize == 4) p[3] = (byte)0;
                    //            p += pixelsize;
                    //            //bmp_source.SetPixel(x, y, Color.White);
                    //        }
                    //        p += nOffset;
                    //    }
                    //}
                }

                //return new Bitmap(bmp_source as Image, size);
                return bmp_source;

            }
            catch (Exception) { return new Bitmap(32, 32); }
            finally
            {
                bmp_source.UnlockBits(bmd);
            }
        }
    }
}
