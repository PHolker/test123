﻿/********************************** Module Header **********************************\
Module Name:  FileThumbnailProvider.cs
Project:      CSShellExtThumbnailHandler
Copyright (c) Microsoft Corporation.

The FileThumbnailProvider.cs file defines a thumbnail handler by implementing the 
IInitializeWithStream and IThumbnailProvider interfaces.

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER 
EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF 
MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***********************************************************************************/

using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Xml.Linq;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Media;
using WVEThumbnail;

namespace WVEShellExt
{
    [ClassInterface(ClassInterfaceType.None)]
    [ProgId("WVE.preview"), Guid("E0AD31BE-C995-4469-85F0-4495BA3F5A33"), ComVisible(true)]
    public class ThumbnailProvider : IInitializeWithStream, IThumbnailProvider
    {

        // Provided during initialization.
        private ReadOnlyIStreamStream _stream = null;
        //private IStream ms = null;

        #region Shell Extension Registration

        [ComRegisterFunction()]
        public static void Register(Type t)
        {
            try
            {
                ShellExtReg.RegisterShellExtThumbnailHandler(t.GUID, ".wve");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Log the error
                throw;  // Re-throw the exception
            }
        }

        [ComUnregisterFunction()]
        public static void Unregister(Type t)
        {
            try
            {
                ShellExtReg.UnregisterShellExtThumbnailHandler(".wve");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message); // Log the error
                throw;  // Re-throw the exception
            }
        }

        #endregion


        #region IInitializeWithStream Members

        /// <summary>
        /// Initializes the thumbnail handler with a stream.
        /// </summary>
        /// <param name="stream">The stream source</param>
        /// <param name="grfMode">
        /// One of the following STGM values that indicates the access mode for the 
        /// stream.
        ///   STGM_READ - The stream is read-only.
        ///   STGM_READWRITE - The stream is read/write accessible.
        /// </param>
        public void Initialize(IStream stream, STGM grfMode)
        {
            // A handler instance should be initialized only once in its lifetime. 
            if (this._stream == null)
            {
                // Initialize the stream if it has not been initialized yet.
                this._stream = new ReadOnlyIStreamStream(stream);
                //this.ms = stream;
            }
            else
            {
                Marshal.ThrowExceptionForHR(WinError.HRESULT_FROM_WIN32(
                    WinError.ERROR_ALREADY_INITIALIZED));
            }
        }

        #endregion


        #region IThumbnailProvider Members

       

        /// <summary>
        /// The ThumbnailProvider returns a stream that we convert to a byte array.
        /// </summary>
        /// <returns></returns>
        protected byte[] GetStreamContents()
        {
            //IStream _stream;
            //System.Diagnostics.Debug.Assert(this._streamy != null);

            // How big do we think this stream is?
            //System.Runtime.InteropServices.ComTypes.STATSTG statData;
            //ms.Stat(out statData, 1); // 1 means don't bother with the name.
            //byte[] bytes = new byte[statData.cbSize];
            //IntPtr p = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(UInt64)));
            //ms.Read(bytes, bytes.Length, p);
            //Marshal.FreeCoTaskMem(p);

            // Allocate some memory.
            byte[] bytes = new byte[_stream.Length];// statData.cbSize];
            _stream.Read(bytes, 0, bytes.Length);
            
            
            

            return (bytes);
        }

        
        /// <summary>
        /// Gets a thumbnail image and alpha type. The GetThumbnail is called with 
        /// the largest desired size of the image, in pixels. Although the parameter 
        /// is called cx, this is used as the maximum size of both the x and y 
        /// dimensions. If the retrieved thumbnail is not square, then the longer 
        /// axis is limited by cx and the aspect ratio of the original image 
        /// respected. On exit, GetThumbnail provides a handle to the retrieved image. 
        /// It also provides a value that indicates the color format of the image and 
        /// whether it has valid alpha information.
        /// </summary>
        /// <param name="cx">The maximum thumbnail size, in pixels.</param>
        /// <param name="hbmp">Contains thumbnail image handle.</param>
        /// <param name="dwAlpha">Contains the alpha type.</param>
        public void GetThumbnail(uint cx, out IntPtr hbmp, out WTS_ALPHATYPE dwAlpha)
        {


            #region template

            //// Read the base64-encoded string value representing the thumbnail image 
            //// from the source stream.
            //XDocument doc = XDocument.Load(this._stream);
            //var picture = doc.Element("Recipe").Element("Attachments").Element("Picture");
            //// The XML may provide images of different sizes, and the code can query 
            //// image matching the desired size (cx). For simplicity, this sample 
            //// omits the cx paramter and provides only one image for all situations.
            //string imageString = picture.Attribute("Source").Value;

            //// Convert the base64-encoded string to a stream.
            //byte[] buffer = Convert.FromBase64String(imageString);
            //Stream imageStream = new MemoryStream(buffer);

            //// Construct a bitmap from the stream and get its handle.
            //Bitmap bmp = new Bitmap(imageStream);
            //hbmp = bmp.GetHbitmap();

            //// Set the alpha type of the thumbnail image.
            //dwAlpha = WTS_ALPHATYPE.WTSAT_ARGB;
            #endregion


            #region red square
            //dwAlpha = WTS_ALPHATYPE.WTSAT_ARGB;
            //using (var Thumbnail = new Bitmap((int)cx, (int)cx, PixelFormat.Format32bppArgb))
            //{
            //    using (var G = Graphics.FromImage(Thumbnail))
            //    {
            //        G.Clear(Color.Red);
            //    }
            //    hbmp = Thumbnail.GetHbitmap();
            //    //MessageBox.Show("generating " + cx.ToString() + "px thumbnail", (IntPtr.Size * 8).ToString() + "bit WVEShellExt.ThumbnailProvider", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //}
            //return;
            #endregion


            #region WVE method

            dwAlpha = WTS_ALPHATYPE.WTSAT_ARGB;
            hbmp = IntPtr.Zero;
            try
            {
                //play sound upon thumbnail generation
                //Stream str = Properties.Resources.droplet;
                //SoundPlayer snd = new SoundPlayer(str);
                //snd.Play();
                C_WVE_read wve = new C_WVE_read();

                byte[] file = GetStreamContents();
                bool ok = wve.M_WVE_lesen(file);

                //popup messagebox for Shell debugging only
                //MessageBox.Show("generating " + cx.ToString() + "px thumbnail from " + wve._Col.ToString() + "x" + wve._Col.ToString() + "px data", (IntPtr.Size * 8).ToString() + "bit WVEShellExt.ThumbnailProvider", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (ok)//size > 0)
                {
                    Bitmap Thumbnail = Bitmapper.CreateBMPSource(wve._zWerte, wve._Min, wve._Max);
                    hbmp = Thumbnail.GetHbitmap();
                    //hbmp = Bitmapper.ResizeBitmap(Thumbnail, (int)cx, (int)cx).GetHbitmap();
                }
            }
            catch { } // A dirty cop-out.
            finally
            {
                //Bug fix that prevented loading files in SWPF.CCP/WinRFSA while previewing: release COM object explicitely
                _stream.Dispose(); 
                GC.Collect();
            }
            #endregion
        }

        #endregion
    }
}