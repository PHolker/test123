﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Runtime.InteropServices;

namespace WVEShellExt
{
    /// <summary>
    /// Registration tweak for Windows 7 / VS2010 Setup
    /// </summary>
    [RunInstaller(true)]
    public partial class C_RegisterCOM : System.Configuration.Install.Installer
    {
        // Visual Studio 2010 Setup (VSI) registration is buggy for .NET/x64 assemblies:
        // This registers COM during setup and removal and creates HKCR\.wve tree 
        // VSI create HKCR\CLSID\{GUID} tree 

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);

            RegistrationServices regsrv = new RegistrationServices();
            if (!regsrv.RegisterAssembly(GetType().Assembly, AssemblyRegistrationFlags.SetCodeBase))
            {
                throw new InstallException("Failed to register for COM Interop.");
            }
            //// Get the location of regasm 
            //string regasmPath = System.Runtime.InteropServices.RuntimeEnvironment.GetRuntimeDirectory() + @"regasm.exe";
            //// Get the location of our DLL
            //string componentPath = typeof(RegisterAssembly).Assembly.Location; 
            //// Execute regasm 
            //System.Diagnostics.Process.Start(regasmPath, "/codebase /tlb \"" + componentPath + "\"");
        }

        [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand)]
        public override void Uninstall(IDictionary savedState)
        {
            base.Uninstall(savedState);

            RegistrationServices regsrv = new RegistrationServices();
            if (!regsrv.UnregisterAssembly(GetType().Assembly))
            {
                throw new InstallException("Failed to unregister for COM Interop.");
            }
        }


        public C_RegisterCOM()
        {
            InitializeComponent();
        }
    }
}
