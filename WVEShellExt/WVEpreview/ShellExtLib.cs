﻿/********************************** Module Header **********************************\
Module Name:  ShellExtLib.cs
Project:      CSShellExtThumbnailHandler
Copyright (c) Microsoft Corporation.

The file declares the imported Shell interfaces: IThumbnailProvider and 
IInitializeWithStream and implements the helper functions for registering and 
unregistering a shell thumbnail handler.

This source is subject to the Microsoft Public License.
See http://www.microsoft.com/opensource/licenses.mspx#Ms-PL.
All other rights reserved.

THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER 
EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF 
MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
\***********************************************************************************/

using System;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.Win32;
using System.IO;

namespace WVEShellExt
{
    #region Shell Interfaces

    [ComImport(), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("e357fccd-a995-4576-b01f-234630154e96")]
    internal interface IThumbnailProvider
    {
        void GetThumbnail(uint cx, out IntPtr hbmp, out WTS_ALPHATYPE WTSAT_UNKNOWN);
    }


    [ComImport(), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("b824b49d-22ac-4161-ac8a-9916e8fa3f7f")]
    internal interface IInitializeWithStream
    {
        void Initialize(IStream stream, STGM grfMode);
    }

    #endregion


    #region Shell Registration

    internal class ShellExtReg
    {
        /// <summary>
        /// Register the shell thumbnail handler.
        /// </summary>
        /// <param name="clsid">The CLSID of the component.</param>
        /// <param name="fileType">
        /// The file type that the infotip handler is associated with. For 
        /// example, '*' means all file types; '.txt' means all .txt files. The 
        /// parameter must not be NULL or an empty string. 
        /// </param>
        /// <remarks>
        /// The function creates the following key in the registry.
        ///
        ///   HKCR
        ///   {
        ///      NoRemove &lt;File Type&gt;
        ///      {
        ///          NoRemove shellex
        ///          {
        ///              {e357fccd-a995-4576-b01f-234630154e96} = s '{&lt;CLSID&gt;}'
        ///          }
        ///      }
        ///   }
        /// </remarks>
        public static void RegisterShellExtThumbnailHandler(Guid clsid, string fileType)
        {
            if (clsid == Guid.Empty)
            {
                throw new ArgumentException("clsid must not be empty");
            }
            if (string.IsNullOrEmpty(fileType))
            {
                throw new ArgumentException("fileType must not be null or empty");
            }

            // If fileType starts with '.', try to read the default value of the 
            // HKCR\<File Type> key which contains the ProgID to which the file type 
            // is linked.
            if (fileType.StartsWith("."))
            {
                using (RegistryKey key = Registry.ClassesRoot.OpenSubKey(fileType))
                {
                    if (key != null)
                    {
                        // If the key exists and its default value is not empty, use 
                        // the ProgID as the file type.
                        string defaultVal = key.GetValue(null) as string;
                        if (!string.IsNullOrEmpty(defaultVal))
                        {
                            fileType = defaultVal;
                        }
                    }
                }
            }

            // Create the registry key 
            // HKCR\<File Type>\shellex\{e357fccd-a995-4576-b01f-234630154e96}, and 
            // sets its default value to the CLSID of the handler.
            string keyName = string.Format(
                @"{0}\shellex\{{e357fccd-a995-4576-b01f-234630154e96}}", fileType);
            using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(keyName))
            {
                // Set the default value of the key.
                if (key != null)
                {
                    key.SetValue(null, clsid.ToString("B"));
                }
            }

            // Create the registry key (not tested!)
            // HKLM\Software\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved\clsid
            //keyName = string.Format(
            //    @"{2}\Software\Microsoft\Windows\CurrentVersion\Shell Extensions\Approved\"+clsid.ToString(), fileType);
            //using (RegistryKey key = Registry.ClassesRoot.CreateSubKey(keyName))
            //{
            //    // Set the default value of the key.
            //    if (key != null)
            //    {
            //        key.SetValue(null, clsid.ToString("B"));
            //    }
            //}

            // This tells the shell to invalidate the thumbnail cache. It is important 
            // because any .recipe files viewed before registering this handler would 
            // otherwise show cached blank thumbnails.
            NativeMethods.SHChangeNotify(HChangeNotifyEventID.SHCNE_ASSOCCHANGED, 
                HChangeNotifyFlags.SHCNF_IDLIST, IntPtr.Zero, IntPtr.Zero);
        }

        /// <summary>
        /// Unregister the shell thumbnail handler.
        /// </summary>
        /// <param name="fileType">
        /// The file type that the context menu handler is associated with. For 
        /// example, '*' means all file types; '.txt' means all .txt files. The 
        /// parameter must not be NULL or an empty string. 
        /// </param>
        /// <remarks>
        /// The method removes the registry key 
        /// HKCR\&lt;File Type&gt;\shellex\{e357fccd-a995-4576-b01f-234630154e96}.
        /// </remarks>
        public static void UnregisterShellExtThumbnailHandler(string fileType)
        {
            if (string.IsNullOrEmpty(fileType))
            {
                throw new ArgumentException("fileType must not be null or empty");
            }

            // If fileType starts with '.', try to read the default value of the 
            // HKCR\<File Type> key which contains the ProgID to which the file type 
            // is linked.
            if (fileType.StartsWith("."))
            {
                using (RegistryKey key = Registry.ClassesRoot.OpenSubKey(fileType))
                {
                    if (key != null)
                    {
                        // If the key exists and its default value is not empty, use 
                        // the ProgID as the file type.
                        string defaultVal = key.GetValue(null) as string;
                        if (!string.IsNullOrEmpty(defaultVal))
                        {
                            fileType = defaultVal;
                        }
                    }
                }
            }

            // Remove the registry key:
            // HKCR\<File Type>\shellex\{e357fccd-a995-4576-b01f-234630154e96}.
            string keyName = string.Format(
                @"{0}\shellex\{{e357fccd-a995-4576-b01f-234630154e96}}", fileType);
            Registry.ClassesRoot.DeleteSubKeyTree(keyName, false);
        }
    }

    #endregion


    #region Enums & Structs

    public enum WTS_ALPHATYPE
    {
        WTSAT_UNKNOWN = 0,
        WTSAT_RGB,
        WTSAT_ARGB
    }

    public enum STGM : uint
    {
        STGM_READ = 0,
        STGM_READWRITE = 2
    }

    [Flags]
    internal enum HChangeNotifyEventID
    {
        SHCNE_RENAMEITEM = 0x00000001,
        SHCNE_CREATE = 0x00000002,
        SHCNE_DELETE = 0x00000004,
        SHCNE_MKDIR = 0x00000008,
        SHCNE_RMDIR = 0x00000010,
        SHCNE_MEDIAINSERTED = 0x00000020,
        SHCNE_MEDIAREMOVED = 0x00000040,
        SHCNE_DRIVEREMOVED = 0x00000080,
        SHCNE_DRIVEADD = 0x00000100,
        SHCNE_NETSHARE = 0x00000200,
        SHCNE_NETUNSHARE = 0x00000400,
        SHCNE_ATTRIBUTES = 0x00000800,
        SHCNE_UPDATEDIR = 0x00001000,
        SHCNE_SERVERDISCONNECT = 0x00004000,
        SHCNE_UPDATEIMAGE = 0x00008000,
        SHCNE_DRIVEADDGUI = 0x00010000,
        SHCNE_RENAMEFOLDER = 0x00020000,
        SHCNE_FREESPACE = 0x00040000,
        SHCNE_EXTENDED_EVENT = 0x04000000,
        SHCNE_ASSOCCHANGED = 0x08000000,
        SHCNE_ALLEVENTS = 0x7FFFFFFF,
    }

    internal enum HChangeNotifyFlags
    {
        SHCNF_IDLIST = 0x0000,
        SHCNF_PATHA = 0x0001,
        SHCNF_PRINTERA = 0x0002,
        SHCNF_DWORD = 0x0003,        
        SHCNF_PATHW = 0x0005,
        SHCNF_PRINTERW = 0x0006,
        SHCNF_FLUSH = 0x1000,
        SHCNF_FLUSHNOWAIT = 0x2000
    }

    #endregion


    internal class ReadOnlyIStreamStream : Stream
    {
        private IStream _stream;

        public ReadOnlyIStreamStream(IStream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            _stream = stream;
        }

        protected override void Dispose(bool disposing)
        {
            if (_stream != null)
            {
                //Bug fix that prevented loading files in SWPF.CCP/WinRFSA while previewing: release COM object explicitely
                Marshal.ReleaseComObject(_stream);
                _stream = null;
            }
            //_stream = null;
            base.Dispose(disposing);
        }

        private void ThrowIfDisposed()
        {
            if (_stream == null)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }

        
        /// <summary>
        /// Reads a specified number of bytes from the stream object into memory starting
        /// at the current seek pointer.
        /// </summary>
        /// <param name="buffer">When this method returns, contains the data read from the stream. 
        /// This parameter is passed uninitialized.</param>
        /// <param name="offset"></param>
        /// <param name="count">The number of bytes to read from the stream object.</param>
        /// <returns></returns>
        public override int Read(byte[] buffer, int offset, int count)
        {
            ThrowIfDisposed();

            if (buffer == null)
            {
                throw new ArgumentNullException("buffer");
            }

            if (offset < 0)
            {
                throw new ArgumentNullException("offset");
            }

            if (count < 0)
            {
                throw new ArgumentNullException("count");
            }

            int bytesRead = 0;
            if (count > 0)
            {
                object boxedBytesRead = bytesRead;
                GCHandle gch = GCHandle.Alloc(boxedBytesRead, GCHandleType.Pinned);
                try
                {
                    IntPtr ptr = gch.AddrOfPinnedObject();
                    if (offset == 0)
                    {
                        if (count > buffer.Length)
                        {
                            throw new ArgumentOutOfRangeException("count");
                        }

                        _stream.Read(buffer, count, ptr);
                        bytesRead = Convert.ToInt32(boxedBytesRead);
                    }
                    else
                    {
                        byte[] tempBuffer = new byte[count];
                        _stream.Read(tempBuffer, count, ptr);
                        bytesRead = Convert.ToInt32(boxedBytesRead);
                        if (bytesRead > 0)
                        {
                            Array.Copy(tempBuffer, 0, buffer, offset, bytesRead);
                        }
                    }
                }
                finally
                {
                    if (gch != null && gch.IsAllocated)
                    {
                        gch.Free();
                    }
                }
            }
            return bytesRead;
        }

        public override bool CanRead
        {
            get { return _stream != null; }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            ThrowIfDisposed();

            long pos = 0;
            object boxedPos = pos;
            GCHandle gch = GCHandle.Alloc(boxedPos, GCHandleType.Pinned);
            try
            {
                IntPtr posPtr = gch.AddrOfPinnedObject();
                _stream.Seek(offset, (int)origin, posPtr);
                pos = Convert.ToInt64(boxedPos);
            }
            finally
            {
                if (gch != null && gch.IsAllocated)
                {
                    gch.Free();
                }
            }
            return pos;
        }

        public override bool CanSeek
        {
            get { return _stream != null; }
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            ThrowIfDisposed();
            throw new NotSupportedException();
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override long Length
        {
            get
            {
                ThrowIfDisposed();
                const int STATFLAG_NONAME = 1;
                System.Runtime.InteropServices.ComTypes.STATSTG stats;
                _stream.Stat(out stats, STATFLAG_NONAME);
                return stats.cbSize;
            }
        }

        public override void SetLength(long value)
        {
            ThrowIfDisposed();
            throw new NotSupportedException();
        }

        public override long Position
        {
            get
            {
                ThrowIfDisposed();
                return Seek(0, SeekOrigin.Current);
            }
            set
            {
                ThrowIfDisposed();
                Seek(value, SeekOrigin.Begin);
            }
        }

        public override void Flush()
        {
            ThrowIfDisposed();
        }        
    }


    internal class NativeMethods
    {
        /// <summary>
        /// Notifies the system of an event that an application has performed. An 
        /// application should use this function if it performs an action that may 
        /// affect the Shell.
        /// </summary>
        /// <param name="wEventId">Describes the event that has occurred.</param>
        /// <param name="uFlags">
        /// Flags that, when combined bitwise with SHCNF_TYPE, indicate the 
        /// meaning of the dwItem1 and dwItem2 parameters.
        /// </param>
        /// <param name="dwItem1">Optional. First event-dependent value.</param>
        /// <param name="dwItem2">Optional. Second event-dependent value.</param>
        [DllImport("shell32.dll", CharSet = CharSet.Unicode)]
        public static extern void SHChangeNotify(
            HChangeNotifyEventID wEventId, 
            HChangeNotifyFlags uFlags,
            IntPtr dwItem1,
            IntPtr dwItem2);
    }


    internal static class WinError
    {
        public const int ERROR_ALREADY_INITIALIZED = 0x4df;

        /// <summary>
        /// Maps a system error code to an HRESULT value.
        /// </summary>
        /// <param name="win32Error">Win32 error code</param>
        /// <returns>HRESULT</returns>
        public static int HRESULT_FROM_WIN32(int win32Error)
        {
            if ((win32Error & 0x80000000L) == 0x80000000L)
            {
                return win32Error;
            }
            return ((win32Error & 0xffff) | -2147024896);
        }
    }
}