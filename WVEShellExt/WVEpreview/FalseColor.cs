﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace WVEThumbnail
{
    public class FalseColors
    {
        double[] A_f_Steps = new double[4];
        private double d_SetMinZ;
        public double P_SetMinZ
        {
            set { d_SetMinZ = value; }
        }

        private double d_SetMaxZ;
        public double P_SetMaxZ
        {
            set { d_SetMaxZ = value; }
        }

        private double d_SetMaxZ2;
        public double P_SetMaxZ2
        {
            set { d_SetMaxZ2 = value; }
        }

        private double d_SetMaxZ3;
        public double P_SetMaxZ3
        {
            set { d_SetMaxZ3 = value; }
        }

        private double d_SetMaxZ4;
        public double P_SetMaxZ4
        {
            set { d_SetMaxZ4 = value; }
        }

        private double d_SetMaxZ5;
        public double P_SetMaxZ5
        {
            set { d_SetMaxZ5 = value; }
        }

        private double d_SetMaxZ6;
        public double P_SetMaxZ6
        {
            set { d_SetMaxZ6 = value; }
        }

        public FalseColors()
        {

            A_f_Steps[0] = 0.125;
            A_f_Steps[1] = 0.375;
            A_f_Steps[2] = 0.625;
            A_f_Steps[3] = 0.875;
        }

        public Color GetColor(double f_Parameter)
        {	// Test if we are in range.
            if (double.IsNaN(f_Parameter))
                return Color.White;

            double deltaZ = d_SetMaxZ - d_SetMinZ;

            double d_x = (f_Parameter - d_SetMinZ) / deltaZ;

            if (deltaZ < 0.0)
                return Color.Blue;
            else if (deltaZ == 0.0)
                d_x = 0.5;

            // Compute x value.


            // See in which step we are.
            if (d_x < 0)
                return Color.FromArgb(0, 0, 128);

            if (d_x < A_f_Steps[0])
                return Color.FromArgb(0, 0, (int)(127.0 / A_f_Steps[0] * d_x + 128.0));

            if (d_x < A_f_Steps[1])
                return Color.FromArgb(0, (int)((d_x - A_f_Steps[0]) * 255.0 / (A_f_Steps[1] - A_f_Steps[0])), 255);

            if (d_x < A_f_Steps[2])
            {
                double d = 255.0 / (A_f_Steps[2] - A_f_Steps[1]);
                return Color.FromArgb((int)((d_x - A_f_Steps[1]) * d), 255, (int)((A_f_Steps[2] - d_x) * d));
            }
            if (d_x < A_f_Steps[3])
                return Color.FromArgb(255, (int)((A_f_Steps[3] - d_x) / (A_f_Steps[3] - A_f_Steps[2]) * 255.0), 0);

            if (d_x < 1)
            {
                double d = 127.0 / (1.0 - A_f_Steps[3]);
                return Color.FromArgb((int)(128.0 + d - d_x * d), 0, 0);
            }// Ouside
            return Color.FromArgb(128, 0, 0);
        }

    }
}
