﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace WVEThumbnail
{
    public class C_WVE_read
    {
        #region Member und Properties
        private string PartNo = null;
        private string Date = null;
        private string UpDate = null;
        private string User = null;
        private string Comment = null;
        private string Type = null;
        private bool Error = false;
        private int X0 = 0;
        private int Y0 = 0;
        private int Col = 0;
        private int Row = 0;
        private double Min = 0;
        private double Max = 0;
        private double rms = double.NaN;
        private double[,] Trans = null;
        private double[] xK = null;
        private double[] yK = null;
        private double[,] zWerte = null;
        private bool[,] zWerte_bool = null;
        private int AnzGültigerPunkte = 0;

        public string _PartNo { get { return this.PartNo; } set { this.PartNo = value; } }
        public string _Date { get { return this.Date; } set { this.Date = value; } }
        public string _UpDate { get { return this.UpDate; } set { this.UpDate = value; } }
        public string _User { get { return this.User; } set { this.User = value; } }
        public string _Comment { get { return this.Comment; } set { this.Comment = value; } }
        public string _Type { get { return this.Type; } set { this.Type = value; } }
        public bool _Error { get { return this.Error; } set { this.Error = value; } }
        public int _X0 { get { return this.X0; } set { this.X0 = value; } }
        public int _Y0 { get { return this.Y0; } set { this.Y0 = value; } }
        public int _Col { get { return this.Col; } set { this.Col = value; } }
        public int _Row { get { return this.Row; } set { this.Row = value; } }
        public double _Min { get { return this.Min; } set { this.Min = value; } }
        public double _Max { get { return this.Max; } set { this.Max = value; } }
        public double _rms { get { return this.GetRMS(); } set { this.rms = value; } }

        private double GetRMS()
        {
            if (!double.IsNaN(this.rms))
            {
                return (this.rms);
            }
            else
            {
                int i, j, npts = 0;
                int Zeilen = this.zWerte.GetLength(0);
                int Spalten = this.zWerte.GetLength(1);
                double Mittel = 0.0f;

                double rms = double.NaN;

                for (i = 0; i < Zeilen; i++)
                    for (j = 0; j < Spalten; j++)
                        if (!double.IsNaN(this.zWerte[i, j]))
                        {
                            Mittel += this.zWerte[i, j];
                            npts++;
                        }
                // Berechnung der mittleren verbleibenden Fehlerhöhe, um RMS zu berechnen		
                Mittel /= (double)(npts);
                double drms = 0.0;
                for (i = 0; i < Zeilen; i++)
                {
                    for (j = 0; j < Spalten; j++)
                    {
                        if (!double.IsNaN(this.zWerte[i, j]))
                        {
                            drms += (Math.Pow((this.zWerte[i, j] - Mittel), 2.0));
                        }
                    }
                }
                if (npts != 0)
                {
                    rms = (double)Math.Sqrt(drms / (double)npts);
                    this.rms = rms;
                }
                return (rms);
            }
        }
        public double _X { get { return (this.Trans != null) ? this.Trans[3, 0] * this.Col : 0; } set { if (this.Trans != null) this.Trans[3, 0] = value; } }
        public double _Y { get { return (this.Trans != null) ? this.Trans[3, 1] * this.Row : 0; } set { if (this.Trans != null) this.Trans[3, 1] = value; } }
        public double[,] _Trans { get { return (this.Trans == null) ? null : (double[,])this.Trans.Clone(); } set { this.Trans = value; } }
        public double[] _xK { get { return this.xK; } set { this.xK = value; } }
        public double[] _yK { get { return this.yK; } set { this.yK = value; } }
        public double[,] _zWerte { get { return this.zWerte; } set { this.zWerte = value; } }
        public bool[,] _zWerte_bool { get { return this.zWerte_bool; } set { this.zWerte_bool = value; } }
        public int _AnzGültigerPunkte { get { return this.AnzGültigerPunkte; } set { this.AnzGültigerPunkte = value; } }
        #endregion

        public C_WVE_read() { }

        #region private methods
        /// <summary>
        /// clip colors
        /// </summary>
        /// <param name="High"></param>
        /// <param name="Low"></param>
        /// <returns></returns>
        internal System.UInt16 M_WertIWord(byte High, byte Low)
        {
            return (System.UInt16)(High * 256 + Low);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="Beginn"></param>
        /// <returns></returns>
        internal System.UInt32 M_WertIDWord(ref byte[] raw, int Beginn)
        {
            return M_WertIDWord(raw[Beginn], raw[Beginn + 1], raw[Beginn + 2], raw[Beginn + 3]);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="Beginn"></param>
        /// <returns></returns>
        internal System.UInt16 M_WertIWord(ref byte[] raw, int Beginn)
        {
            return M_WertIWord(raw[Beginn], raw[Beginn + 1]);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="High"></param>
        /// <param name="High2"></param>
        /// <param name="Low2"></param>
        /// <param name="Low"></param>
        /// <returns></returns>
        internal System.UInt32 M_WertIDWord(byte High, byte High2, byte Low2, byte Low)
        {
            return (System.UInt32)(M_WertIWord(High, High2) * (65536) + M_WertIWord(Low2, Low));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Character"></param>
        /// <returns></returns>
        internal System.Char M_WertCByteASCII(byte Character)
        {
            return System.Convert.ToChar(Character);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="Beginn"></param>
        /// <param name="Laenge"></param>
        /// <returns></returns>
        internal string M_StringASCII(ref byte[] raw, int Beginn, int Laenge)
        {
            StringBuilder strb = new StringBuilder();
            for (int a = 0; a < Laenge; a++)
            {
                strb.Append(M_WertCByteASCII(raw[Beginn + a]));
            }

            return strb.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="raw"></param>
        /// <param name="Beginn"></param>
        /// <returns></returns>
        protected double M_DoubleE136(ref byte[] raw, int Beginn)
        {

            string s = System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator;
            return System.Convert.ToDouble(M_StringASCII(ref raw, Beginn, 13).Replace(".", s));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bit8"></param>
        /// <returns></returns>
        protected byte M_ByteFromString(string bit8)
        {
            byte res = 0;
            if (bit8 != null && bit8.Length == 8)
            {
                for (int i = 0; i < 8; i++)
                {
                    res *= 2;
                    res += bit8[i] == '1' ? (byte)1 : (byte)0;
                }

            }

            return res;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="toRev"></param>
        /// <returns></returns>
        protected byte M_Reverse(byte toRev)
        {
            byte res = 0;
            byte v = 0;
            for (int i = 0; i < 8; i++)
            {
                v = (byte)(toRev % 2);
                toRev /= 2;
                res *= 2;
                res += v;
            }
            return res;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private double[] M_GetXCoordinates()
        {
            if (this.zWerte != null)
            {
                double[] res = new double[this.Col];
                double dx = this._X / this.Col;
                for (int i = 0; i < this.Col; i++)
                {
                    res[i] = -(this.Col - 1) * (dx / 2.0) + i * dx;
                }
                return res;
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private double[] M_GetYCoordinates()
        {
            if (this.zWerte != null)
            {
                double[] res = new double[this.Row];
                double dy = this._Y / this.Row;
                for (int i = 0; i < this.Row; i++)
                {
                    res[i] = -(this.Row - 1) * (dy / 2.0) + i * dy;
                }
                return res;
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region public methods
        /// <summary>
        /// Methode, welche D100-Datei ausliest und Feld erzeugt sowie belegt
        /// </summary>
        /// <param name="Filename"></param>
        /// <returns></returns>
        public bool M_WVE_lesen(byte[] raw)
        {
            try
            {
                //if (File.Exists(Filename))
                //{
                //    if (Path.GetExtension(Filename).Equals(".wve") || Path.GetExtension(Filename).Equals(".WVE"))
                #region wve
                    //    {
                       try
                       {
                           #region via WVEFileStream
                           //WVEFileStream wveData = new WVEFileStream(Filename);
                            //this.Row = wveData.GetMatrixRow();
                            //this.Col = wveData.GetMatrixColumn();
                            //this.zWerte = wveData.GetMatrix();
                            //this.xK = wveData.GetXCoordinate();				// [mm]
                            //this.yK = wveData.GetYCoordinate();

                            //this.PartNo = wveData.GetTeileNummerValue();
                            //this.Date = wveData.GetDateValue();
                            //this.UpDate = wveData.GetDateValue();
                            //this.User = wveData.GetUserValue();
                            //this.Comment = wveData.GetKommentarValue();
                            //this.X0 = wveData.GetlX0();
                            //this.Y0 = wveData.GetlY0();
                            ////this.Min = wveData.;
                            ////this.Max = 0;
                            ////this.Trans = wveData.GetT;
                            ////private bool[,] zWerte_bool = wveData.;
                            //this.AnzGültigerPunkte = 0;

                            //this.zWerte_bool = new bool[this.Row, this.Col];
                            //for (int i = 0; i < this.Row; i++)
                            //{
                            //    for (int j = 0; j < this.Col; j++)
                            //    {
                            //        if (!double.IsNaN(this.zWerte[i,j]))
                            //        {
                            //            this.zWerte_bool[i, j] = true;
                            //        }
                            //    }
                            //}
                            #endregion
                           
                            #region auskommentiert: Methode von Mirko
                            //this.Filename = System.IO.Path.GetFileName(Filename);
                            this.Error = false;
                            //byte[] raw = File.ReadAllBytes(Filename);
                            uint V = M_WertIDWord(ref raw, 0x0);
                            if (V == 0x30470006 || V == 0x30480006 /* || V == 0x30488006*/)//WVE-Version 2.2 -- Da nur diese unterstützt wird
                            {
                                //Dinge die nach Definition folgen werden nicht ausgelesen, genauso wie Userdatenblock
                                #region Beginn Datei-Header 0x000
                                int o_comment = (int)M_WertIDWord(ref raw, 0x004);
                                //int l_comment = (int)M_WertIDWord(ref raw, 0x008);
                                int o_datahead = (int)M_WertIDWord(ref raw, 0x00C); //bei WVE folgen auf den Datenkopf direkt die Daten
                                //int l_datahead = (int)M_WertIDWord(ref raw, 0x010);
                                //int o_data = (int)M_WertIDWord(ref raw, 0x014);
                                //int l_data = (int)M_WertIDWord(ref raw, 0x018);
                                //int o_user = (int)M_WertIDWord(ref raw, 0x01C);
                                //int l_user = (int)M_WertIDWord(ref raw, 0x020);
                                #endregion //Ende Datei-Header 0x024
                                #region Dekomprimieren
                                //if (V == 0x30488006) //Komprimiert
                                //{
                                //    //Dekomprimieren
                                //    //Alles ab W_Data Header
                                //    int addrdec= o_datahead + 0x10A;
                                //    List<byte> lb = new List<byte>();
                                //    int l = raw.GetLength(0);
                                //    StringBuilder sb = new StringBuilder();
                                //    for (int al = 0; al < l; al++)
                                //    {
                                //        if (al < addrdec)
                                //        {
                                //            lb.Add(raw[al]);
                                //        }
                                //        else
                                //        {
                                //            //Huffman gleich auflösen
                                //            if (al != addrdec)
                                //            {
                                //                sb.Append(HuffmannNo1Back(raw[al]));
                                //            }

                                //        }


                                //    }
                                //    //toDec wiederherstellen
                                //    string sh = sb.ToString();

                                //    List<byte> toDec = new List<byte>();
                                //    int shl = sh.Length / 8;
                                //    for (int a = 0; a < shl; a++)
                                //    {
                                //        toDec.Add(M_ByteFromString(sh.Substring(a*8,8)));
                                //    }
                                //    //LZ77 rückgängig
                                //    //W_Bad Abfolgen
                                //    raw = lb.ToArray();
                                //};
                                #endregion
                                #region Kommentarblock
                                int iKomH = o_comment;
                                int iKomH2 = M_WertIWord(ref raw, iKomH);
                                iKomH += 2;
                                this.PartNo = M_StringASCII(ref raw, iKomH, iKomH2);

                                iKomH += iKomH2;
                                iKomH2 = M_WertIWord(ref raw, iKomH);
                                iKomH += 2;
                                this.Date = M_StringASCII(ref raw, iKomH, iKomH2);

                                iKomH += iKomH2;
                                iKomH2 = M_WertIWord(ref raw, iKomH);
                                iKomH += 2;
                                this.UpDate = M_StringASCII(ref raw, iKomH, iKomH2);

                                iKomH += iKomH2;
                                iKomH2 = M_WertIWord(ref raw, iKomH);
                                iKomH += 2;
                                this.User = M_StringASCII(ref raw, iKomH, iKomH2);

                                iKomH += iKomH2;
                                iKomH2 = M_WertIWord(ref raw, iKomH);
                                iKomH += 2;
                                this.Comment = M_StringASCII(ref raw, iKomH, iKomH2);

                                iKomH += iKomH2;
                                iKomH2 = M_WertIWord(ref raw, iKomH);
                                iKomH += 2;
                                this.Type = M_StringASCII(ref raw, iKomH, iKomH2);
                                if (this.Type.Length > 40)
                                {
                                    this.Type = "unbekannt";
                                }


                                #endregion //Ende Kommentarblock
                                #region Datenheader
                                this.X0 = (System.Int16)M_WertIWord(ref raw, o_datahead + 0x000);
                                this.Y0 = (System.Int16)M_WertIWord(ref raw, o_datahead + 0x002);
                                //flipped VKu
                                this.Col = M_WertIWord(ref raw, o_datahead + 0x004);
                                this.Row = M_WertIWord(ref raw, o_datahead + 0x006);
                                UInt16 Bad = M_WertIWord(ref raw, o_datahead + 0x008);
                                this.Min = M_DoubleE136(ref raw, o_datahead + 0x00E);
                                this.Max = M_DoubleE136(ref raw, o_datahead + 0x01C);
                                this.Trans = new double[4, 4];
                                int ft = o_datahead + 0x02A;
                                for (int tz = 0; tz < 4; tz++)
                                {
                                    for (int ts = 0; ts < 4; ts++)
                                    {
                                        this.Trans[tz, ts] = M_DoubleE136(ref raw, ft);
                                        ft += 14;
                                    }
                                }
                                #endregion Datenheader
                                #region Daten
                                int d = o_datahead + 0x10A;
                                this.xK = new double[this.Col];
                                this.yK = new double[this.Row];
                                this.zWerte = new double[this.Row, this.Col];
                                this.zWerte_bool = new bool[this.Row, this.Col];
                                ushort hw = 0;
                                this.AnzGültigerPunkte = 0;
                                for (int r = 0; r < this.Row; r++)
                                {
                                    for (int z = 0; z < this.Col; z++)
                                    {
                                        hw = M_WertIWord(ref raw, d);
                                        if (hw != Bad)
                                        {
                                            this.zWerte[r, z] = (hw * (this.Max - this.Min)) / 65534.0 + this.Min;
                                            this.zWerte_bool[r, z] = true;
                                            this.AnzGültigerPunkte++;
                                        }
                                        else
                                        {
                                            this.zWerte[r, z] = Double.NaN;
                                            this.zWerte_bool[r, z] = false;
                                        }
                                        d += 2;//zum nächsten
                                    }
                                }
                                this.xK = this.M_GetXCoordinates();
                                this.yK = this.M_GetYCoordinates();
                                #endregion //Daten
                            }
                            else
                            {
                                Error = true;
                            }
                            #endregion


                        }
                        catch
                        {
                            Error = true;
                        }
                #endregion
                    
                //else if (Path.GetExtension(Filename).Equals(".dat") || Path.GetExtension(Filename).Equals(".ascii") || Path.GetExtension(Filename).Equals(".asc") || Path.GetExtension(Filename).Equals(".xyz"))
                #region dat/ascii/xyz
                    //{
                    
                    //    int i, j;
                    //    int count = 0;
                    //    string zeile = "";
                    //    double[,] Werte_tmp = new double[0, 0];
                    //    string trennzeichen = " ";
                    //    double faktor = 1.0;

                    //    using (C_Trennzeichen_Form t_form = new C_Trennzeichen_Form())
                    //    {
                    //        t_form.ShowDialog();

                    //        if (t_form.rb_tabstopp.Checked)
                    //            trennzeichen = "\t";
                    //        else if (t_form.rb_leerzeichen.Checked)
                    //            trennzeichen = " ";
                    //        else if (t_form.rb_semikolon.Checked)
                    //            trennzeichen = ";";
                    //        else if (t_form.rb_komma.Checked)
                    //            trennzeichen = ",";
                    //        else if (t_form.rb_andere.Checked)
                    //            trennzeichen = t_form.tb_trennzeichen.Text;

                    //        if (t_form.rb_millimeter.Checked)
                    //            faktor = 1000.0;
                    //        else if (t_form.rb_mikrometer.Checked)
                    //            faktor = 1.0;
                    //        else if (t_form.rb_nanometer.Checked)
                    //            faktor = 0.001;
                    //    }

                    //    using (FileStream stream = new FileStream(Filename, FileMode.OpenOrCreate, FileAccess.Read))
                    //    {
                    //        using (StreamReader sr = new StreamReader(stream, System.Text.Encoding.Default))
                    //        {
                    //            //File bis zu Ende lesen
                    //            while (!sr.EndOfStream)
                    //            {
                    //                //Zeile für Zeile lesen
                    //                zeile = sr.ReadLine().Trim();					// Lesen Zeile mit Werte
                    //                //Wenn die Zeile leer ist, dann ist die auflistung der Messdaten zu Ende
                    //                if (!zeile.Equals(""))
                    //                {
                    //                    count++;
                    //                }
                    //            }

                    //            if (count < 25)
                    //                throw new Exception("Die Anzahl der XYZ-Punkte ist zu klein.\r\nStellen Sie sicher dass mindestens" +
                    //                    "5x5 XYZ-Tupel => d.h. 25 Punkte vorhanden sind.");

                    //            Werte_tmp = new double[count, 3];
                    //            stream.Seek((long)0, SeekOrigin.Begin);

                    //            i = 0;
                    //            //File bis zu Ende lesen
                    //            while (!sr.EndOfStream)
                    //            {
                    //                //Zeile für Zeile lesen
                    //                zeile = sr.ReadLine().Trim();					// Lesen Zeile mit Werte

                    //                zeile = zeile.Replace(trennzeichen, " "); // trennzeichen durch leerzeichen ersetzen
                    //                zeile = zeile.Replace("  ", " "); // 2 leerzeichen durch 1 ersetzen
                    //                zeile = zeile.Replace("  ", " "); // 2 leerzeichen durch 1 ersetzen

                    //                //Wenn die Zeile leer ist, dann ist die auflistung der Messdaten zu Ende
                    //                if (!zeile.Equals(""))
                    //                {
                    //                    string[] zeile_temp_s = zeile.Split(" ".ToCharArray()); // nach Leerzeichen (Trennzeichen) splitten

                    //                    if (zeile_temp_s.Length != 3)
                    //                        throw new Exception("Es konnten keine 3 Spalten ermittelt werden! Bitte Trennzeichen überprüfen!");

                    //                    // X-Wert
                    //                    Werte_tmp[i, 0] = Convert.ToDouble(zeile_temp_s[0]);
                    //                    // Y-Wert
                    //                    Werte_tmp[i, 1] = Convert.ToDouble(zeile_temp_s[1]);
                    //                    // Z-Wert
                    //                    Werte_tmp[i, 2] = Convert.ToDouble(zeile_temp_s[2]) * faktor;
                    //                    i++;
                    //                }
                    //            }
                    //        }
                    //    }

                    //    //bestimme XCoord und YCoord
                    //    double x_min = double.MaxValue;
                    //    double x_max = double.MinValue;
                    //    double y_min = double.MaxValue;
                    //    double y_max = double.MinValue;
                    //    for (i = 0; i < count; i++)
                    //    {
                    //        x_min = Math.Min(x_min, Werte_tmp[i, 0]);
                    //        x_max = Math.Max(x_max, Werte_tmp[i, 0]);
                    //        y_min = Math.Min(y_min, Werte_tmp[i, 1]);
                    //        y_max = Math.Max(y_max, Werte_tmp[i, 1]);
                    //    }
                    //    double dx = double.MaxValue;
                    //    double dy = double.MaxValue;
                    //    for (i = 0; i < count; i++)
                    //    {
                    //        if (Werte_tmp[i, 0] != Werte_tmp[0, 0]) if (Math.Abs(Werte_tmp[i, 0] - Werte_tmp[0, 0]) > 0.01) dx = Math.Min(dx, Math.Abs(Werte_tmp[i, 0] - Werte_tmp[0, 0]));
                    //        if (Werte_tmp[i, 1] != Werte_tmp[0, 1]) if (Math.Abs(Werte_tmp[i, 1] - Werte_tmp[0, 1]) > 0.01) dy = Math.Min(dy, Math.Abs(Werte_tmp[i, 1] - Werte_tmp[0, 1]));
                    //    }
                    //    int idim = (int)((y_max - y_min + dy / 100.0) / dy) + 1;
                    //    int jdim = (int)((x_max - x_min + dx / 100.0) / dx) + 1;

                    //    double[] temp_xK = new double[jdim];
                    //    for (j = 0; j < jdim; j++) temp_xK[j] = x_min + (double)j * dx;

                    //    double[] temp_yK = new double[idim];
                    //    for (i = 0; i < idim; i++) temp_yK[i] = y_min + (double)i * dy;

                    //    double[,] temp_zWerte = new double[idim, jdim];
                    //    bool[,] temp_zWerte_bool = new bool[idim, jdim];
                    //    for (i = 0; i < idim; i++) for (j = 0; j < jdim; j++) { temp_zWerte[i, j] = double.NaN; temp_zWerte_bool[i, j] = false; }

                    //    for (i = 0; i < count; i++)
                    //    {
                    //        temp_zWerte[(int)((Werte_tmp[i, 1] - y_min + dy / 100.0) / dy), (int)((Werte_tmp[i, 0] - x_min + dx / 100.0) / dx)] = Werte_tmp[i, 2];
                    //        if (!double.IsNaN(temp_zWerte[(int)((Werte_tmp[i, 1] - y_min + dy / 100.0) / dy), (int)((Werte_tmp[i, 0] - x_min + dx / 100.0) / dx)])) temp_zWerte_bool[(int)((Werte_tmp[i, 1] - y_min + dy / 100.0) / dy), (int)((Werte_tmp[i, 0] - x_min + dx / 100.0) / dx)] = true;
                    //    }
                    //    this.xK = (double[])temp_xK.Clone();
                    //    this.yK = (double[])temp_yK.Clone();
                    //    this.zWerte = (double[,])temp_zWerte.Clone();
                    //    this.zWerte_bool = (bool[,])temp_zWerte_bool.Clone();

                    //    for (i = 0; i < this.yK.Length; i++)
                    //    {
                    //        for (j = 0; j < this.xK.Length; j++)
                    //        {
                    //            //this.zWerte[i, j] = temp_zWerte[this.yK.Length - i - 1, j];
                    //            //this.zWerte_bool[i, j] = temp_zWerte_bool[this.yK.Length - i - 1, j];

                    //            this.zWerte[i, j] = temp_zWerte[i, j];
                    //            this.zWerte_bool[i, j] = temp_zWerte_bool[i, j];
                    //        }
                    //    }

                    //    return true;

                    //}
                #endregion
                return (true);
            }
            catch
            {
                return (false);
            }
        }
        #endregion
    }
}
